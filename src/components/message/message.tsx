import * as React from 'react';
import * as PropTypes from 'prop-types';
import './styles.less';

const getClassName = (resultOfLastTurn: string) => {
  switch (resultOfLastTurn) {
    case 'hit':
      return 'hit message'
    case 'miss':
      return 'miss message'
    default:
      return 'message'
  }
}

interface MessageProps {
  text: string;
  resultOfLastTurn: string;
}

export const Message: React.SFC<MessageProps> = ({text, resultOfLastTurn}) => {
  console.log('result', resultOfLastTurn)
  return (
    <h2 className={getClassName(resultOfLastTurn)}>{text}</h2>
  )
}

Message.propTypes = {
  text: PropTypes.string,
  resultOfLastTurn: PropTypes.string
}