import * as React from 'react';
import * as PropTypes from 'prop-types';
import { Tile } from '../tile/tile';
import './styles.less';

interface GameBoardProps {
  onClick(tileCoords?: string): void;
  shipPositions: any;
}

interface GameBoardState {
  tiles: any;
}

export class GameBoard extends React.Component<GameBoardProps, GameBoardState> {

  static propTypes = {
    onClick: PropTypes.func,
    shipPositions: PropTypes.object
  }

  constructor() {
    super()
    this.state = {
      tiles: [] as any
    }
  }

  componentDidMount() {
    const tiles = this.generateCoords(100, 9);
    this.setState({tiles});
  }

  private generateCoords = (numberOfCoords: number, maxCoordsPerRow: number) => {
    let coords = [];
    let x = 0;
    let y = 0;
    
    for(let i = 0; i < numberOfCoords; i++) {
      // Capture the current coordinate
      const coord = {x, y};
      // If x equals 9, start a new row
      if (x === maxCoordsPerRow) {
        x = 0;
        y = y + 1;
      } else {
        x++;
      }
      
      coords.push(coord)
    }
    return coords;
  }

  render() {
    const { onClick, shipPositions } = this.props;
    const { tiles } = this.state;
    return (
      <div className="card">
        <div className="game-board">
          {
            tiles.length > 0
            ? tiles.map((coords: any, i: number) => {
                return (
                  <Tile coords={coords} onClick={onClick} shipPositions={shipPositions} key={i} />
                )
              })
            : null
          }
        </div>
      </div>
    )
  }
}