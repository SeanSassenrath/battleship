import * as React from 'react';
import * as PropTypes from 'prop-types';
import './styles.less';

interface TileProps {
  coords: { x: number, y: number };
  onClick(tileCoords?: string): void;
  shipPositions: any;
}

interface TileState {
  status: string | null;
}

export class Tile extends React.Component<TileProps, TileState> {

  static propTypes = {
    coords: PropTypes.object,
    onClick: PropTypes.func,
    shipPositions: PropTypes.object
  }

  constructor() {
    super();
    this.state = {
      status: null,
    }
  }

  private onTileClick = (coords: string) => {
    const { shipPositions } = this.props;

    if (shipPositions.hasOwnProperty(coords)) {
      this.setState({ status: "hit"});
      this.props.onClick(coords)
      return;
    }
    this.setState({ status: "miss" })
    this.props.onClick();
  }

  private setStyle = () => {
    const { status } = this.state;

    switch (status) {
      case "miss":
        return "tile miss"
      case "hit":
        return "tile hit"
      default:
        return "tile"
    }
  }

  render() {
    const { x, y } = this.props.coords;
    const { status } = this.state;

    return (
      <button 
        className={this.setStyle()} 
        onClick={() => this.onTileClick(`${y}${x}`)}
        disabled={status === "hit" || status === "miss"}
      >
        <p>{`${y}, ${x}`}</p>
      </button>
    )
  }
}