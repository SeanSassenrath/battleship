import * as React from 'react';
import * as PropTypes from 'prop-types';
import './score-board.less';

const renderShipsStatus = (shipsStatus: any) => {
  return shipsStatus.map((ship: any, i: number) => (
    <li key={i}>
      <p className={ship.sunk ? 'sunk-ship' : 'ship'}>{ship.name}</p>
    </li>
  ))
}

interface ScoreBoardProps {
  shipsStatus: any;
  hitCount: number;
  missCount: number;
}

export const ScoreBoard: React.SFC<ScoreBoardProps> = (props) => {
  const { shipsStatus, hitCount, missCount } = props;
  const totalCount = hitCount + missCount;
  const accuracy = Math.floor((hitCount / totalCount) * 100);
  return (
    <div className="score-board">
      <div className="stats-container">
        <h3 className="list-title">Ship Status</h3>
        <ul className="ship-status-list list">
          { renderShipsStatus(shipsStatus) }
        </ul>
      </div>
      <div className="stats-container">
        <h3 className="list-title">Game stats</h3>
        <ul className="list">
          <li>
            <p>{`Accuracy: ${isNaN(accuracy) ? 0 : accuracy}%`}</p>
          </li>
          <li>
            <p>{`Hits: ${hitCount}`}</p>
          </li>
          <li>
            <p>{`Misses: ${missCount}`}</p>
          </li>
        </ul>
      </div>
    </div>
  )
}

ScoreBoard.propTypes = {
  shipsStatus: PropTypes.array,
  hitCount: PropTypes.number,
  missCount: PropTypes.number
}