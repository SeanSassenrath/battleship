import * as React from 'react';
import * as PropTypes from 'prop-types';
import { GameBoard } from './components/game-board/game-board';
import { ScoreBoard } from './components/score-board/score-board';
import { Message } from './components/message/message';
import { data } from './data';
import './styles.less';

interface AppState {
  missCount: number;
  hitCount: number;
  message: string;
  shipsStatus: any;
  shipPositions: any;
  resultOfLastTurn: string;
}

export class App extends React.Component<any, AppState> {

  static propTypes = {
    missCount: PropTypes.number,
    hitCount: PropTypes.number,
    message: PropTypes.string,
    shipsStatus: PropTypes.array,
    shipPositions: PropTypes.object,
  }

  constructor() {
    super();
    this.state = {
      shipsStatus: this.getShipStatus(data),
      shipPositions: { ...this.getShipCoords(data) },
      message: 'Select a tile to get started!',
      hitCount: 0,
      missCount: 0,
      resultOfLastTurn: null
    }
  }

  private getShipStatus = ({ shipTypes }: any) => {
    const shipStatusList = [];
    const shipStatus = {} as any;
    for(var shipName in shipTypes) {
      shipStatusList.push(
        {
          name: shipName,
          size: shipTypes[shipName].size,
          hits: 0,
          sunk: false
        }
      )
    }
    return shipStatusList;
  }

  private getShipCoords = ({ layout }: any) => {
    const shipCoords = {} as any;
    layout.map((shipLayout: any) => {
      // Get the name of the ship
      const ship = shipLayout.ship;
      // Get each ship position and set it as a key
      shipLayout.positions.map((position: any) => {
        const shipPosition = position.join("");
        shipCoords[shipPosition] = { ship, hit: false };
      })
    })
    return shipCoords;
  }

  private findStatusOfHitShip = (shipsStatus: any, hitShipPosition: any) => {
    return shipsStatus.find((ship: any) => ship.name === hitShipPosition.ship)
  }

  private determineMessage = (shipPositions: any, hitShipStatus: any, hitCount: any) => {
    switch (true) {
      case Object.keys(shipPositions).length - 1 <= hitCount:
        return 'game over!';
      case hitShipStatus.hits >= hitShipStatus.size - 1:
        return `${hitShipStatus.name}, sunk!`;
      default:
        return `${hitShipStatus.name}, hit!`
    }
  }

  private caculateNewStateOnHit = (tileCoords: string) => {
    const { hitCount, missCount, shipsStatus, shipPositions } = this.state;

    const hitShipPosition = shipPositions[tileCoords];
    const hitShipStatus = this.findStatusOfHitShip(shipsStatus, hitShipPosition);
    const shipStatusMessage = this.determineMessage(shipPositions, hitShipStatus, hitCount);
    
    const newState = Object.assign(
      this.state, 
      hitShipPosition.hit = true,
      // Add to the hit count
      hitShipStatus.hits = hitShipStatus.hits + 1,
      // Determine if the ship has been sunk
      hitShipStatus.sunk = hitShipStatus.hits >= hitShipStatus.size
    );

    this.setState(prevState => ({
      newState,
      message: shipStatusMessage,
      hitCount: prevState.hitCount += 1,
      resultOfLastTurn: 'hit'
    }));
  }

  private onClick = (tileCoords: string) => {
    const { hitCount, missCount, shipsStatus, shipPositions } = this.state;

    if (shipPositions.hasOwnProperty(tileCoords)) {
      this.caculateNewStateOnHit(tileCoords);
      return;
    }

    this.setState(prevState => ({ 
      message: 'miss!',
      missCount: prevState.missCount += 1,
      resultOfLastTurn: 'miss'
    }))
  }

  render() {
    const { hitCount, missCount, message, resultOfLastTurn ,shipPositions, shipsStatus } = this.state;
    return (
      <div className="app">
        <h1 className="title">BattleShip</h1>
        <Message text={message} resultOfLastTurn={resultOfLastTurn}/>
        <GameBoard onClick={this.onClick} shipPositions={shipPositions} />
        <ScoreBoard shipsStatus={shipsStatus} hitCount={hitCount} missCount={missCount} />
      </div>
    )
  }
}