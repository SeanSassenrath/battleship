const webpack = require('webpack');
const { resolve } = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  devtool: 'cheap-eval-source-map',
  entry: {
    bundle: [
      './src/index.tsx',
    ],
    vendor: [
      'react',
      'react-dom',
    ]
  },
  output: {
    filename: '[name].js',
    path: resolve(__dirname, 'dist'),
    publicPath: '/'
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js']
  },
  module: {
    rules: [
      { 
        test: /\.tsx?$/,
        loader: ['ts-loader'],
        exclude: /(node_modules)/,
      }, 
      {
        test:/\.less?$/,
        loader: "style-loader!css-loader!less-loader",
        exclude: /(node_modules)/,
      },
      {
        enforce: 'pre',
        test: /\.js?$/,
        loader: 'source-map-loader'
      }
    ]
  },
  plugins: [
    new HTMLWebpackPlugin({
      template: './index.tmpl.html',
      filename: 'index.html',
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: ['vendor', 'manifest'],
    }),
  ],
  devServer: {
    contentBase: resolve(__dirname, 'dist'),
    compress: true,
    port: 9000,
    publicPath: '/',
  }
};